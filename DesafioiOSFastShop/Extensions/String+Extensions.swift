//
//  String+Extensions.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 26/05/23.
//

import Foundation

extension String {

    static func getPrices(_ prices: [MarvelComicsDataResultsPriesModel]?) -> String {
        guard let comicPrices = prices else { return "" }
        var resultsPrices = [String]()

        for price in comicPrices {
            var formattedPrice = "\(price.type): $\(price.price)\n"
            formattedPrice = formattedPrice.replacingOccurrences(of: "printPrice", with: "Price")
            formattedPrice = formattedPrice.replacingOccurrences(of: "digitalPurchasePrice", with: "Digital Price")
            resultsPrices.append(formattedPrice)
        }

        return resultsPrices.joined(separator: "")
    }

    static func getThumbnailFullPath(_ path: String, _ pathExtension: String) -> String {
        return "\(path).\(pathExtension)".replacingOccurrences(of: "http://", with: "https://")
    }

    static func getIssue(_ issue: Double) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal

        guard let formatedIssue = numberFormatter.string(from: NSNumber(value: issue)) else { return "Issue -"}

        return "Issue #\(formatedIssue)"
    }
}
