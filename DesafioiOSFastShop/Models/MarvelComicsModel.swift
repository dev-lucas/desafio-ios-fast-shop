//
//  MarvelComicsModel.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 26/05/23.
//

import UIKit

struct MarvelComicsModel: Decodable {
    let data: MarvelComicsDataModel
}

struct MarvelComicsDataModel: Decodable {
    let offset: Int
    let results: [MarvelComicsDataResultsModel]
}

struct MarvelComicsDataResultsModel: Decodable {
    let title: String
    let issueNumber: Double
    let prices: [MarvelComicsDataResultsPriesModel]
    let thumbnail: MarvelComicsDataResultsThumbnailModel
}

struct MarvelComicsDataResultsPriesModel: Decodable {
    let type: String
    let price: Double
}

struct MarvelComicsDataResultsThumbnailModel: Decodable {
    let path: String
    let `extension`: String
}
