//
//  SceneDelegate.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 25/05/23.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }

        window = UIWindow(windowScene: windowScene)

        let defaults = UserDefaults.standard
        let userAuthenticated = defaults.bool(forKey: "userAuthenticated")

        if userAuthenticated {
            let tabBarController = createTabBarController()
            setRootViewController(tabBarController)
        } else {
            let loginViewController = LoginViewController()
            window?.rootViewController = loginViewController
        }

        window?.makeKeyAndVisible()
    }

    func setRootViewController(_ viewController: UIViewController) {
        guard let window = window else {
            return
        }

        window.rootViewController = viewController
    }

    func createTabBarController() -> UITabBarController {
        let tabBarController = UITabBarController()

        let listViewController = ListViewController()
        let favoritesViewController = FavoritesViewController()
        let cartViewController = CartViewController()

        listViewController.tabBarItem = UITabBarItem(title: "Home", image: UIImage(systemName: "house"), tag: 0)
        favoritesViewController.tabBarItem = UITabBarItem(title: "Favoritos", image: UIImage(systemName: "heart"), tag: 1)
        cartViewController.tabBarItem = UITabBarItem(title: "Carrinho", image: UIImage(systemName: "cart"), tag: 2)

        let navigationController1 = UINavigationController(rootViewController: listViewController)
        let navigationController2 = UINavigationController(rootViewController: favoritesViewController)
        let navigationController3 = UINavigationController(rootViewController: cartViewController)

        tabBarController.viewControllers = [navigationController1, navigationController2, navigationController3]

        return tabBarController
    }
}
