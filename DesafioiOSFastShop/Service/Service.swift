//
//  Service.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 26/05/23.
//

import UIKit
import Alamofire
import AlamofireImage

class Service: NSObject {
    private let url: String = "https://gateway.marvel.com/v1/public/comics"
    private let ts = "1674566400"
    private let apiKey = "7303f17f9a1578113e4ccc981fac7043"
    private let md5 = "035f48dad054dad47d6e29c7d651905f"
    private let format = "comic"

    func getComics(title: String = "", offset: Int = 0, dateRange: String = "1939-01-01%2C2023-01-01", completion: @escaping (Result<MarvelComicsModel, Error>) -> Void) {
        let titleParam = validateTitleString(title)

        AF.request(
            "\(url)?ts=\(ts)&apikey=\(apiKey)&hash=\(md5)&format=\(format)&dateRange=\(dateRange)&offset=\(offset)\(titleParam)",
            method: .get,
            encoding: JSONEncoding.default
        )
        .responseDecodable(of: MarvelComicsModel.self) { response in
            switch response.result {
            case .success(let result):
                completion(.success(result))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    static func setImageViewByUrl(_ imageView: UIImageView?, _ url: String?) {
        guard let imageView = imageView,
              let urlString = url,
                let imageURL = URL(string: urlString) else {
            imageView?.image = nil
            return
        }

        imageView.af.setImage(withURL: imageURL)
    }

    private func validateTitleString(_ string: String) -> String {
        let trimmedString = string.trimmingCharacters(in: .whitespaces)

        if trimmedString.isEmpty {
            return ""
        }

        let allowedCharacterSet = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~")

        if let encodedString = trimmedString.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
            return "&titleStartsWith=\(encodedString)"
        } else {
            return ""
        }
    }
}
