//
//  StoreManager.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 27/05/23.
//

import Foundation
import StoreKit

class StoreManager: NSObject {

    static let shared = StoreManager()
    var products = [SKProduct]()

    func purchaseProduct(_ product: SKProduct) {
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
}

extension StoreManager: SKProductsRequestDelegate {

    enum Product: String, CaseIterable {
        case hqSpiderMan = "com.myapp.spiderman"
        case hqHulk = "com.myapp.hulk"
        case hqIronMan = "com.myapp.ironman"
    }

    func fetchProducts() {
        let request = SKProductsRequest(productIdentifiers: Set(Product.allCases.compactMap({ $0.rawValue })))
        request.delegate = self
        request.start()
    }

    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        products = response.products
    }
}
