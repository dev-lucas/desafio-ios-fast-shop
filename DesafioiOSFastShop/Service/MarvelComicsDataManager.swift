//
//  MarvelComicsDataManager.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 27/05/23.
//

import UIKit

struct MarvelComicsDataManagerModel: Codable {
    let title: String
    let issueNumber: Double
    let price: Double?
}

class MarvelComicsDataManager {
    static let shared = MarvelComicsDataManager()
    private let userDefaults = UserDefaults.standard
    private let favoritesKey = "MarvelComicsFavorites"
    private let cartKey = "MarvelComicsCart"

    private var favorites: [MarvelComicsDataManagerModel] {
        get {
            guard let data = userDefaults.data(forKey: favoritesKey),
                  let favorites = try? JSONDecoder().decode([MarvelComicsDataManagerModel].self, from: data) else {
                return []
            }
            return favorites
        }
        set {
            let encodedData = try? JSONEncoder().encode(newValue)
            userDefaults.set(encodedData, forKey: favoritesKey)
        }
    }

    private var cart: [MarvelComicsDataManagerModel] {
        get {
            guard let data = userDefaults.data(forKey: cartKey),
                  let favorites = try? JSONDecoder().decode([MarvelComicsDataManagerModel].self, from: data) else {
                return []
            }
            return favorites
        }
        set {
            let encodedData = try? JSONEncoder().encode(newValue)
            userDefaults.set(encodedData, forKey: cartKey)
        }
    }

    func saveFavorites(_ comic: MarvelComicsDataManagerModel) {
        var currentFavorites = favorites
        currentFavorites.append(comic)
        favorites = currentFavorites
    }

    func deleteFavorites(_ comic: MarvelComicsDataManagerModel) {
        var currentFavorites = favorites
        currentFavorites.removeAll { $0.title == comic.title && $0.issueNumber == comic.issueNumber }
        favorites = currentFavorites
    }

    func getFavorites(_ comic: MarvelComicsDataManagerModel) -> MarvelComicsDataManagerModel? {
        return favorites.first { $0.title == comic.title && $0.issueNumber == comic.issueNumber }
    }

    func loadMarvelComics() -> [MarvelComicsDataManagerModel] {
        return favorites
    }

    func saveCart(_ comic: MarvelComicsDataManagerModel) {
        var currentCart = cart
        currentCart.append(comic)
        cart = currentCart
    }

    func deleteCart(_ comic: MarvelComicsDataManagerModel) {
        if let index = cart.firstIndex(where: { $0.title == comic.title && $0.issueNumber == comic.issueNumber }) {
            cart.remove(at: index)
        }
    }

    func getCart(_ comic: MarvelComicsDataManagerModel) -> MarvelComicsDataManagerModel? {
        return cart.first { $0.title == comic.title && $0.issueNumber == comic.issueNumber }
    }

    func loadCart() -> [MarvelComicsDataManagerModel] {
        return cart
    }
}
