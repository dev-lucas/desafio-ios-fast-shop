//
//  LoginView.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 26/05/23.
//

import UIKit

class LoginView: UIView {

    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 30
        stackView.backgroundColor = .systemBackground
        return stackView
    }()

    lazy var image: UIImageView = {
        let image = UIImageView(image: UIImage(named: "marvel-logo"))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.clipsToBounds = true
        return image
    }()

    lazy var emailTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Email"
        textField.borderStyle = .roundedRect
        textField.autocapitalizationType = .none
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()

    lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Password"
        textField.borderStyle = .roundedRect
        textField.isSecureTextEntry = true
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()

    lazy var buttonView: ButtonView = {
        let button = ButtonView()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension LoginView {
    func setupViews() {
        self.backgroundColor = .systemBackground
        self.configureSubviews()
        self.configureSubviewsConstraints()
    }

    private func configureSubviews() {
        addSubview(mainStackView)
        mainStackView.addArrangedSubview(image)
        mainStackView.addArrangedSubview(emailTextField)
        mainStackView.addArrangedSubview(passwordTextField)
        mainStackView.addArrangedSubview(buttonView)
     }

    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            mainStackView.widthAnchor.constraint(equalToConstant: 240),
            mainStackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            mainStackView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -100),
            image.heightAnchor.constraint(equalToConstant: 40),
            emailTextField.heightAnchor.constraint(equalToConstant: 40),
            passwordTextField.heightAnchor.constraint(equalToConstant: 40),
            buttonView.widthAnchor.constraint(equalToConstant: 240),
            buttonView.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
}
