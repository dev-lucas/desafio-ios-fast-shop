//
//  LoginViewController.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 26/05/23.
//

import UIKit

class LoginViewController: UIViewController {
    private let viewModel = LoginViewModel()
    private let loginView: LoginView = {
        let loginView = LoginView()
        return loginView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func loadView() {
        self.view = loginView
    }

    func setupView() {
        loginView.buttonView.updateView(with: ButtonViewConfiguration(title: "ENTRAR", action: { [weak self] in
            guard let self = self else { return }

            let email = self.loginView.emailTextField.text
            let password = self.loginView.passwordTextField.text

            self.viewModel.authenticateUser(email: email, password: password) { [weak self] success in
                if success {
                    guard let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else {
                        return
                    }
                    sceneDelegate.setRootViewController(sceneDelegate.createTabBarController())
                } else {
                    let alert = UIAlertController(title: "Login Falhou", message: "E-mail ou senha inválidos.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                }
            }
        }))
    }
}
