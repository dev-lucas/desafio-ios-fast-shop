//
//  LoginViewModel.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 28/05/23.
//

import UIKit

class LoginViewModel {
    private let mockEmail = "teste@teste"
    private let mockPassword = "123"

    func authenticateUser(email: String?, password: String?, completion: @escaping (Bool) -> Void) {
        guard let email = email, let password = password else {
            completion(false)
            return
        }

        if email == mockEmail && password == mockPassword {
            let defaults = UserDefaults.standard
            defaults.set(true, forKey: "userAuthenticated")
            completion(true)
        } else {
            completion(false)
        }
    }
}
