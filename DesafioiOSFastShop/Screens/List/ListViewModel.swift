//
//  ListViewModel.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 28/05/23.
//

import UIKit

protocol ListViewModelDelegate: AnyObject {
    func showLoadingView(with configuration: LoadingViewConfiguration)
    func showEmptyView(with configuration: EmptyViewConfiguration)
    func showListView()
    func reloadListView()
    func startInfiniteScrollSpiner()
    func stopInfiniteScrollSpiner()
}

class ListViewModel {
    weak var delegate: ListViewModelDelegate?

    var listItens: [MarvelComicsDataResultsModel] = []
    private var comicsOffset = 0
    private var comicsTitle = ""
    var hasLoaded = false
    var dateRange = "1939-01-01%2C2023-01-01"

    private let service = Service()

    func viewDidLoad() {
        getComics()
    }

    func searchBarSearchButtonClicked(with text: String?) {
        guard let text = text else { return }
        comicsTitle = text
        getComics()
    }

    func searchBarCancelButtonClicked() {
        comicsOffset = 0
        comicsTitle = ""
        hasLoaded = false
        didSelectYears(startYear: 1939, endYear: 2023)
    }

    func loadMoreData() {
        delegate?.startInfiniteScrollSpiner()
        comicsOffset += 1
        hasLoaded = true

        service.getComics(title: comicsTitle, offset: comicsOffset, dateRange: dateRange) { [weak self] result in
            switch result {
            case .success(let comics):
                if comics.data.results.isEmpty {
                    self?.delegate?.showEmptyView(with: EmptyViewConfiguration(title: "Nenhuma HQ encontrada", subtitle: "Procure por um título diferente, ex Ant-Man!"))
                } else {
                    self?.listItens = (self?.listItens ?? []) + comics.data.results
                    self?.delegate?.reloadListView()
                    self?.hasLoaded = false
                    self?.delegate?.showListView()
                    self?.delegate?.stopInfiniteScrollSpiner()
                }

            case .failure(let error):
                print(error)
                self?.delegate?.showEmptyView(with: EmptyViewConfiguration(title: "Ocorreu um erro", subtitle: error.localizedDescription))
            }
        }
    }

    func didSelectYears(startYear: Int, endYear: Int) {
        dateRange = "\(startYear)-01-01%2C\(endYear)-01-01"
        getComics()
    }

    func getComics() {
        delegate?.showLoadingView(with: LoadingViewConfiguration(title: "Carregando HQ's..."))
        service.getComics(title: comicsTitle, offset: comicsOffset, dateRange: dateRange) { [weak self] result in
            switch result {
            case .success(let comics):
                if comics.data.results.isEmpty {
                    self?.delegate?.showEmptyView(with: EmptyViewConfiguration(title: "Nenhuma HQ encontrada", subtitle: "Procure por um título diferente, ex Ant-Man!\nClique em 'Cancelar' para recarregar"))
                } else {
                    self?.listItens = comics.data.results
                    self?.delegate?.showListView()
                    self?.delegate?.reloadListView()
                }
            case .failure(let error):
                self?.delegate?.showEmptyView(with: EmptyViewConfiguration(title: "Ocorreu um erro", subtitle: error.localizedDescription))
            }
        }
    }

    func numberOfItemsInSection() -> Int {
        return listItens.count
    }

    func itemAtIndexPath(_ indexPath: IndexPath) -> MarvelComicsDataResultsModel? {
        guard indexPath.row < listItens.count else { return nil }
        return listItens[indexPath.row]
    }

    func cellSizeForItemAt(_ indexPath: IndexPath) -> CGSize {
        return CGSize(width: 170, height: 320)
    }

    func didSelectItemAt(_ indexPath: IndexPath) -> DetailsViewController? {
        guard let comic = itemAtIndexPath(indexPath) else { return nil }
        return DetailsViewController.instance(comic)
    }

    func scrollViewDidScroll(contentOffsetY: CGFloat, contentSizeHeight: CGFloat, frameHeight: CGFloat) {
        let maximumOffset = contentSizeHeight - frameHeight

        if contentOffsetY > 0 && maximumOffset > 0 && !hasLoaded && contentOffsetY >= maximumOffset {
            loadMoreData()
        }
    }
}
