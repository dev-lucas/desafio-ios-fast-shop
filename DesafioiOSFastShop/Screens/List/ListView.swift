//
//  ListView.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 25/05/23.
//

import UIKit

final class ListView: UIView {

    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 15
        layout.minimumInteritemSpacing = 15
        layout.sectionInset = UIEdgeInsets(top: 15, left: 15, bottom: 0, right: 15)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .systemGray6
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100.0, right: 0)
        return collectionView
    }()

    lazy var spinnerActivityIndicator: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.hidesWhenStopped = true
        return spinner
    }()

    init() {
        super.init(frame: .zero)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension ListView {

    func setupViews() {
        backgroundColor = .systemGray6
        configureSubviews()
        configureSubviewsConstraints()
    }

    func configureSubviews() {
        addSubview(collectionView)
        addSubview(spinnerActivityIndicator)
    }

    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor),
            spinnerActivityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            spinnerActivityIndicator.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: -130)
        ])
    }
}
