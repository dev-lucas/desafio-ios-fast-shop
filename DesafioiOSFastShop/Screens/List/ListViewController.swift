//
//  ListViewController.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 25/05/23.
//

import UIKit

class ListViewController: UIViewController {

    private let pullToRefreshView: PullToRefreshView = {
        let pullToRefreshView = PullToRefreshView()
        return pullToRefreshView
    }()

    private var listFilterView: ListFilterView = {
        let listFilterView = ListFilterView()
        return listFilterView
    }()

    private let listView: ListView = {
        let listView = ListView()
        return listView
    }()

    private let emptyView: EmptyView = {
        let emptyView = EmptyView()
        return emptyView
    }()

    private let loadingView: LoadingView = {
        let loadingView = LoadingView()
        return loadingView
    }()

    private lazy var searchController: UISearchController = {
        let searchController = UISearchController()
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Filter by Title"
        return searchController
    }()

    private let viewModel = ListViewModel()

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        setupNavigationBar()
        setupCollection()
        viewModel.delegate = self
        viewModel.getComics()
        setupPullToRefresh()
    }

    override func loadView() {
        view = listView
    }

    private func setupNavigationBar() {
        let navBarAppearance = UINavigationBarAppearance()
        let backButton = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: .none)
        let logoutButton = UIBarButtonItem(title: "Sair", style: .plain, target: self, action: #selector(logout))
        let imageView = UIImageView(image: UIImage(named: "marvel-logo"))
        let containerView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 30))

        navBarAppearance.configureWithOpaqueBackground()
        navBarAppearance.backgroundColor = .systemBackground
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: 0, y: 0, width: 100, height: 30)
        containerView.addSubview(imageView)

        navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        navigationItem.backBarButtonItem = backButton
        navigationItem.rightBarButtonItem = logoutButton
        navigationItem.searchController = searchController
        navigationItem.titleView = containerView
    }

    private func setupCollection() {
        listView.collectionView.delegate = self
        listView.collectionView.dataSource = self
        listView.collectionView.register(CardView.self, forCellWithReuseIdentifier: "CardView")
        listView.collectionView.register(ListFilterView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ListFilterView")
    }

    private func setupPullToRefresh() {
        pullToRefreshView.onRefresh = { [weak self] in
            self?.viewModel.getComics()
            self?.pullToRefreshView.endRefreshing()
        }
        pullToRefreshView.addToScrollView(self.listView.collectionView)
    }

    @objc private func logout(sender: UIButton) {
        let defaults = UserDefaults.standard
        defaults.set(false, forKey: "userAuthenticated")
        guard let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else { return }
        sceneDelegate.setRootViewController(UINavigationController(rootViewController: LoginViewController()))
    }
}

extension ListViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewModel.searchBarSearchButtonClicked(with: searchBar.text)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.searchBarCancelButtonClicked()
        listFilterView.resetSelectedYears()
    }
}

extension ListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInSection()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardView", for: indexPath) as? CardView else { fatalError("Error dequeueReusableCell CardView") }
        if let comic = viewModel.itemAtIndexPath(indexPath) {
            cell.updateView(with: CardViewConfiguration(comics: comic))
        }
        return cell
    }
}

extension ListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 170, height: 320)
    }
}

extension ListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailsViewController = DetailsViewController.instance(viewModel.listItens[indexPath.row])
        navigationController?.pushViewController(detailsViewController, animated: true)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffsetY = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height

        if contentOffsetY > 0 &&
            maximumOffset > 0 &&
            !viewModel.hasLoaded &&
            contentOffsetY >= maximumOffset {
            viewModel.loadMoreData()
        }
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ListFilterView", for: indexPath) as? ListFilterView else { fatalError("ListFilterView error") }
        listFilterView = header
        listFilterView.delegate = self
        return header
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.size.width, height: 70)
    }
}

extension ListViewController: ListFilterViewDelegate {
    func didSelectYears(startYear: Int, endYear: Int) {
        viewModel.didSelectYears(startYear: startYear, endYear: endYear)
    }
}

extension ListViewController: ListViewModelDelegate {
    func showLoadingView(with configuration: LoadingViewConfiguration) {
        view = loadingView
        loadingView.updateView(with: configuration)
        loadingView.startAnimating()
    }

    func showEmptyView(with configuration: EmptyViewConfiguration) {
        view = emptyView
        emptyView.updateView(with: configuration)
    }

    func showListView() {
        view = listView
    }

    func reloadListView() {
        listView.collectionView.reloadData()
    }

    func startInfiniteScrollSpiner() {
        listView.spinnerActivityIndicator.startAnimating()
    }

    func stopInfiniteScrollSpiner() {
        listView.spinnerActivityIndicator.stopAnimating()
    }
}
