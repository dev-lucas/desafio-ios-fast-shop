//
//  FavoritesViewController.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 26/05/23.
//

import UIKit

class FavoritesViewController: UIViewController {
    private let viewModel = FavoritesViewModel()
    private let favoritesView: FavoritesView = {
        let favoritesView = FavoritesView()
        return favoritesView
    }()

    private let emptyView: EmptyView = {
        let emptyView = EmptyView()
        return emptyView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupTableView()
        viewModel.loadFavorites()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupFavorites()
    }

    override func loadView() {
        view = favoritesView
    }

    private func setupNavigationBar() {
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
        navigationItem.title = "Favoritos"
        navigationItem.largeTitleDisplayMode = .never
    }

    private func setupTableView() {
        favoritesView.tableView.register(CellView.self, forCellReuseIdentifier: CellView.classIdentifier())
        favoritesView.tableView.dataSource = self
        favoritesView.tableView.delegate = self
        favoritesView.tableView.isEditing = true
    }

    private func setupFavorites() {
        viewModel.loadFavorites()
        if viewModel.favoritesIsEmpty {
            view = emptyView
            emptyView.updateView(with: EmptyViewConfiguration(title: "Nenhuma HQ salva nos favoritos", subtitle: "Favorite as HQ's tocando no coração na dela de detalhes"))
        } else {
            view = favoritesView
            favoritesView.tableView.reloadData()
        }
    }
}

extension FavoritesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.loadedComics.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellView.classIdentifier(), for: indexPath) as? CellView else {
            fatalError("Unable to dequeue CellView.")
        }
        cell.updateView(with: CellViewConfiguration(title: viewModel.loadedComics[indexPath.row].title, subtitle: String.getIssue(viewModel.loadedComics[indexPath.row].issueNumber)))
        return cell
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel.deleteFavorite(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            setupFavorites()
        }
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        viewModel.moveFavorite(from: sourceIndexPath.row, to: destinationIndexPath.row)
    }
}

extension FavoritesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}
