//
//  FavoritesViewModel.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 28/05/23.
//

import Foundation

import UIKit

class FavoritesViewModel {
    var loadedComics: [MarvelComicsDataManagerModel] = []

    var favoritesIsEmpty: Bool {
        return loadedComics.isEmpty
    }

    func loadFavorites() {
        loadedComics = MarvelComicsDataManager.shared.loadMarvelComics()
    }

    func deleteFavorite(at index: Int) {
        let comic = loadedComics[index]
        loadedComics.remove(at: index)
        MarvelComicsDataManager.shared.deleteFavorites(MarvelComicsDataManagerModel(title: comic.title, issueNumber: comic.issueNumber, price: nil))
    }

    func moveFavorite(from sourceIndex: Int, to destinationIndex: Int) {
        let movedItem = loadedComics.remove(at: sourceIndex)
        loadedComics.insert(movedItem, at: destinationIndex)
    }
}
