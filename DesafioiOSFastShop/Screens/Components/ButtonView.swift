//
//  ButtonView.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 26/05/23.
//

import UIKit

struct ButtonViewConfiguration {
    let title: String
    var action: (() -> Void)?
}

final class ButtonView: UIView {

    lazy var button: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = .systemFont(ofSize: UIFont.preferredFont(forTextStyle: .body).pointSize, weight: .bold)
        button.tintColor = .white
        button.backgroundColor = .red
        button.layer.cornerRadius = 5
        button.setTitle("ButtonView", for: .normal)
        return button
    }()

    init() {
        super.init(frame: .zero)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension ButtonView {
    func setupViews() {
        configureSubviews()
        configureSubviewsConstraints()
    }

    func configureSubviews() {
        addSubview(button)
    }

    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            button.leadingAnchor.constraint(equalTo: leadingAnchor),
            button.trailingAnchor.constraint(equalTo: trailingAnchor),
            button.bottomAnchor.constraint(equalTo: bottomAnchor),
            button.topAnchor.constraint(equalTo: topAnchor)
        ])
    }
}

extension ButtonView {
    private struct AssociatedKeys {
        static var action: UInt8 = 0
    }

    func updateView(with configuration: ButtonViewConfiguration) {
        button.setTitle(configuration.title, for: .normal)

        if #available(iOS 14.0, *) {
            if let action = configuration.action {
                button.addAction(UIAction { _ in action() }, for: .touchUpInside)
            }
        } else {
            if let action = configuration.action {
                button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
                objc_setAssociatedObject(button, &AssociatedKeys.action, action, .OBJC_ASSOCIATION_RETAIN)
            }
        }
    }

    @objc private func buttonTapped() {
        if let action = objc_getAssociatedObject(button, &AssociatedKeys.action) as? () -> Void {
            action()
        }
    }
}
