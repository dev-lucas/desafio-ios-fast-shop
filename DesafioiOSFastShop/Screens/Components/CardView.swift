//
//  MagazineCardView.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 25/05/23.
//

import UIKit

struct CardViewConfiguration {
    let comics: MarvelComicsDataResultsModel
}

class CardView: UICollectionViewCell {

    private(set) lazy var image: UIImageView = {
        let image = UIImageView(image: UIImage())
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.backgroundColor = .systemBackground
        return image
    }()

    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: UIFont.preferredFont(forTextStyle: .subheadline).pointSize, weight: .semibold)
        label.textAlignment = .left
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CardView {
    func updateView(with configuration: CardViewConfiguration) {
        titleLabel.text = configuration.comics.title
        Service.setImageViewByUrl(image, String.getThumbnailFullPath(configuration.comics.thumbnail.path, configuration.comics.thumbnail.extension))
    }
}

private extension CardView {
    func setupViews() {
        backgroundColor = .systemBackground
        configureSubviews()
        configureSubviewsConstraints()
    }

    func configureSubviews() {
        addSubview(image)
        addSubview(titleLabel)
    }

    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: topAnchor),
            image.heightAnchor.constraint(equalToConstant: 250),
            image.trailingAnchor.constraint(equalTo: trailingAnchor),
            image.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 5),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
