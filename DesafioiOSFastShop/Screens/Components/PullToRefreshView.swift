//
//  PullToRefreshView.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 28/05/23.
//

import UIKit

class PullToRefreshView: UIView {
    var onRefresh: (() -> Void)?
    private var isRefreshing = false
    private var scrollView: UIScrollView?
    private let refreshControl = UIRefreshControl()
    private let spinner = UIActivityIndicatorView(style: .medium)

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupRefreshControl()
        setupSpinner()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupRefreshControl() {
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
    }

    private func setupSpinner() {
        spinner.hidesWhenStopped = true
    }

    func addToScrollView(_ scrollView: UIScrollView) {
        self.scrollView = scrollView
        scrollView.addSubview(refreshControl)
    }

    func beginRefreshing() {
        isRefreshing = true
        refreshControl.beginRefreshing()
        spinner.startAnimating()
    }

    func endRefreshing() {
        isRefreshing = false
        refreshControl.endRefreshing()
        spinner.stopAnimating()
    }

    @objc private func handleRefresh() {
        guard !isRefreshing else { return }
        isRefreshing = true
        onRefresh?()
    }
}
