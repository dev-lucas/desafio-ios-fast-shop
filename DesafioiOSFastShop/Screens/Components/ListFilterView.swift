//
//  ListFilterView.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 27/05/23.
//

import UIKit

protocol ListFilterViewDelegate: AnyObject {
    func didSelectYears(startYear: Int, endYear: Int)
}

class ListFilterView: UICollectionReusableView {
    weak var delegate: ListFilterViewDelegate?

    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 20
        stackView.distribution = .fillEqually
        stackView.backgroundColor = .clear
        return stackView
    }()

    private lazy var button: UIButton = {
        let button = UIButton(frame: .zero)
        let startYear = 1939
        let endYear = 2023
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = .systemFont(ofSize: UIFont.preferredFont(forTextStyle: .body).pointSize, weight: .bold)
        button.setTitleColor(.label, for: .normal)
        button.backgroundColor = .systemGray5
        button.layer.cornerRadius = 10
        button.setTitle("\(startYear) - \(endYear)", for: .normal)
        button.addTarget(self, action: #selector(popupPicker), for: .touchUpInside)
        return button
    }()

    private lazy var pickerView: UIPickerView = {
        let pickerView = UIPickerView(frame: .zero)
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        pickerView.dataSource = self
        pickerView.delegate = self
        return pickerView
    }()

    private var selectedStartYear: Int = 1939
    private var selectedEndYear: Int = 2023

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func resetSelectedYears() {
        selectedStartYear = 1939
        selectedEndYear = 2023
        pickerView.selectRow(selectedStartYear - 1939, inComponent: 0, animated: false)
        pickerView.selectRow(selectedEndYear - 1939, inComponent: 1, animated: false)
        button.setTitle("\(selectedStartYear) - \(selectedEndYear)", for: .normal)
    }

    @objc private func popupPicker() {
        let alertController = UIAlertController(title: "Selecione os Anos", message: "Selecione os anos para filtrar a pesquisa", preferredStyle: .actionSheet)

        alertController.popoverPresentationController?.sourceView = button
        alertController.popoverPresentationController?.sourceRect = button.bounds

        alertController.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))

        alertController.addAction(UIAlertAction(title: "Selecionar", style: .default, handler: { _ in
            self.selectedStartYear = self.pickerView.selectedRow(inComponent: 0) + 1939
            self.selectedEndYear = self.pickerView.selectedRow(inComponent: 1) + 1939
            self.button.setTitle("\(self.selectedStartYear) - \(self.selectedEndYear)", for: .normal)
            self.delegate?.didSelectYears(startYear: self.selectedStartYear, endYear: self.selectedEndYear)
        }))

        let contentViewController = UIViewController()
        contentViewController.preferredContentSize = CGSize(width: UIScreen.main.bounds.width - 10, height: UIScreen.main.bounds.height / 2)
        contentViewController.view.addSubview(pickerView)
        pickerView.centerXAnchor.constraint(equalTo: contentViewController.view.centerXAnchor).isActive = true
        pickerView.centerYAnchor.constraint(equalTo: contentViewController.view.centerYAnchor).isActive = true

        alertController.setValue(contentViewController, forKey: "contentViewController")

        guard let rootViewController = UIApplication.shared.windows.first?.rootViewController else {
            return
        }

        rootViewController.present(alertController, animated: true, completion: nil)
        pickerView.selectRow(selectedEndYear - 1939, inComponent: 1, animated: false)
    }

    private func setupViews() {
        backgroundColor = .systemBackground
        configureSubviews()
        configureSubviewsConstraints()
    }

    private func configureSubviews() {
        addSubview(mainStackView)
        mainStackView.addArrangedSubview(button)
    }

    private func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            mainStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20)
        ])
    }
}

extension ListFilterView: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // 1939 to 2023
        return 2023 - 1939 + 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row + 1939)"
    }
}
