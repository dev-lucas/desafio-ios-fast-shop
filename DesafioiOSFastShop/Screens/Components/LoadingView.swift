//
//  LoadingView.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 26/05/23.
//

import UIKit

struct LoadingViewConfiguration {
    let title: String
}

final class LoadingView: UIView {

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Carregando HQ's..."
        label.font = .systemFont(ofSize: UIFont.preferredFont(forTextStyle: .headline).pointSize, weight: .semibold)
        label.textColor = .label
        return label
    }()

    private lazy var spinnerActivityIndicator: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.hidesWhenStopped = true
        return spinner
    }()

    init() {
        super.init(frame: .zero)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension LoadingView {
    func setupViews() {
        backgroundColor = .systemBackground
        configureSubviews()
        configureSubviewsConstraints()
    }

    func configureSubviews() {
        addSubview(titleLabel)
        addSubview(spinnerActivityIndicator)
    }

    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            spinnerActivityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            spinnerActivityIndicator.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 14)
        ])
    }
}

extension LoadingView {
    func updateView(with configuration: LoadingViewConfiguration) {
        titleLabel.text = configuration.title
    }

    func startAnimating() {
        spinnerActivityIndicator.startAnimating()
    }

    func stopAnimating() {
        spinnerActivityIndicator.stopAnimating()
    }
}
