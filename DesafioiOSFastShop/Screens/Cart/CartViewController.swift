//
//  CartViewController.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 27/05/23.
//

import UIKit

class CartViewController: UIViewController {
    private let viewModel = CartViewModel()
    private let cartView: CartView = {
        let cartView = CartView()
        return cartView
    }()

    private let emptyView: EmptyView = {
        let emptyView = EmptyView()
        return emptyView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupTableView()
        StoreManager.shared.fetchProducts()
        viewModel.loadCart()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupCart()
    }

    override func loadView() {
        view = cartView
    }

    private func setupNavigationBar() {
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
        navigationItem.title = "Carrinho"
        navigationItem.largeTitleDisplayMode = .never
    }

    private func setupTableView() {
        cartView.tableView.register(CellView.self, forCellReuseIdentifier: CellView.classIdentifier())
        cartView.tableView.dataSource = self
        cartView.tableView.delegate = self
        cartView.tableView.setEditing(true, animated: true)
    }

    private func setupCart() {
        if viewModel.cartIsEmpty {
            view = emptyView
            emptyView.updateView(with: EmptyViewConfiguration(title: "Nenhum item no carrinho", subtitle: "Adicione as HQ's tocando no botão do carrinho na tela de detalhes"))
        } else {
            view = cartView
            cartView.buttonView.updateView(with: ButtonViewConfiguration(title: "FINALIZAR COMPRA", action: { [weak self] in
                self?.viewModel.purchaseRandomProduct()
            }))
            cartView.updateView(with: CartViewConfiguration(total: viewModel.cartTotal))
            cartView.tableView.reloadData()
        }
    }
}

extension CartViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.loadedCart.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellView.classIdentifier(), for: indexPath) as? CellView else {
            fatalError("Unable to dequeue CellView.")
        }
        cell.updateView(with: CellViewConfiguration(title: viewModel.loadedCart[indexPath.row].title, subtitle: "Price: $\(viewModel.loadedCart[indexPath.row].price ?? 0)"))
        return cell
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel.deleteCartItem(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            setupCart()
        }
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        viewModel.moveCartItem(from: sourceIndexPath.row, to: destinationIndexPath.row)
    }
}

extension CartViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}
