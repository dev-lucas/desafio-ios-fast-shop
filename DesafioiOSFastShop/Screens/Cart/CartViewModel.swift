//
//  CartViewModel.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 28/05/23.
//

import UIKit

class CartViewModel {
    var loadedCart: [MarvelComicsDataManagerModel] = []

    var cartIsEmpty: Bool {
        return loadedCart.isEmpty
    }

    var cartTotal: String {
        var total = 0.0
        for cart in loadedCart {
            total += cart.price ?? 0
        }
        return "Total: $\(String(format: "%.2f", total))"
    }

    func loadCart() {
        loadedCart = MarvelComicsDataManager.shared.loadCart()
    }

    func deleteCartItem(at index: Int) {
        let comic = loadedCart[index]
        loadedCart.remove(at: index)
        MarvelComicsDataManager.shared.deleteCart(MarvelComicsDataManagerModel(title: comic.title, issueNumber: comic.issueNumber, price: comic.price))
    }

    func moveCartItem(from sourceIndex: Int, to destinationIndex: Int) {
        let movedItem = loadedCart.remove(at: sourceIndex)
        loadedCart.insert(movedItem, at: destinationIndex)
    }

    func purchaseRandomProduct() {
        if let product = StoreManager.shared.products.randomElement() {
            StoreManager.shared.purchaseProduct(product)
        }
    }
}
