//
//  CartView.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 27/05/23.
//

import UIKit

struct CartViewConfiguration {
    let total: String
}

class CartView: UIView {

    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.tableHeaderView = UIView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 15
        stackView.distribution = .fillEqually
        stackView.backgroundColor = .secondarySystemBackground
        stackView.layer.cornerRadius = 5
        return stackView
    }()

    private lazy var auxiliaryView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.backgroundColor = .clear
        return stackView
    }()

    lazy var buttonView: ButtonView = {
        let button = ButtonView()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.contentMode = .scaleAspectFill
        return button
    }()

    private lazy var totalPriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: UIFont.preferredFont(forTextStyle: .callout).pointSize, weight: .semibold)
        label.textAlignment = .left
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()

    init() {
        super.init(frame: .zero)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CartView {
    func updateView(with configuration: CartViewConfiguration) {
        totalPriceLabel.text = configuration.total
    }
}

private extension CartView {

    func setupViews() {
        backgroundColor = .systemBackground
        configureSubviews()
        configureSubviewsConstraints()
    }

    func configureSubviews() {
        addSubview(mainStackView)
        addSubview(tableView)
        mainStackView.addArrangedSubview(auxiliaryView)
        auxiliaryView.addSubview(totalPriceLabel)
        mainStackView.addArrangedSubview(buttonView)
    }

    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            mainStackView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -20),
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            mainStackView.heightAnchor.constraint(equalToConstant: 100),
            totalPriceLabel.leadingAnchor.constraint(equalTo: auxiliaryView.leadingAnchor, constant: 10),
            totalPriceLabel.trailingAnchor.constraint(equalTo: auxiliaryView.trailingAnchor, constant: -10),
            totalPriceLabel.topAnchor.constraint(equalTo: auxiliaryView.topAnchor, constant: 10),
            totalPriceLabel.bottomAnchor.constraint(equalTo: auxiliaryView.bottomAnchor, constant: -10),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.bottomAnchor.constraint(equalTo: mainStackView.topAnchor)
        ])
    }
}
