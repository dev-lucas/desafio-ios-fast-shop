//
//  DetailsViewController.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 26/05/23.
//

import UIKit

class DetailsViewController: UIViewController {
    private let viewModel: DetailsViewModel

    private let detailsView: DetailsView = {
        let detailsView = DetailsView()
        return detailsView
    }()

    private let service = Service()

    init(comic: MarvelComicsDataResultsModel?) {
        self.viewModel = DetailsViewModel(comic: comic)
        super.init(nibName: String(describing: type(of: self)), bundle: nil)
        self.viewModel.delegate = self
        self.viewModel.setupDetails { response in
            favoriteFeedback(is: response)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupDetails()
        StoreManager.shared.fetchProducts()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.setupDetails { response in
            favoriteFeedback(is: response)
        }
    }

    override func loadView() {
        self.view = detailsView
    }

    static func instance(_ comic: MarvelComicsDataResultsModel) -> DetailsViewController {
        let detailsViewController = DetailsViewController(comic: comic)
        return detailsViewController
    }

    private func setupNavigationBar() {
        navigationItem.title = "BOOK DETAILS"
    }
}

extension DetailsViewController: DetailsViewModelDelegate {
    func didUpdateFavoriteStatus(isFavorite: Bool) {
        favoriteFeedback(is: isFavorite)
    }
}

extension DetailsViewController: DetailsViewDelegate {
    func favoriteComic() {
        viewModel.toggleFavoriteComic()
    }
}

private extension DetailsViewController {
    func setupDetails() {
        guard let comic = viewModel.comic else { return }

        detailsView.delegate = self

        detailsView.cartButtonView.updateView(with: ButtonViewConfiguration(title: "", action: {
            let comicModel = MarvelComicsDataManagerModel(title: comic.title, issueNumber: comic.issueNumber, price: comic.prices.first?.price ?? 0)
            MarvelComicsDataManager.shared.saveCart(comicModel)
            self.navigationController?.pushViewController(CartViewController(), animated: true)
        }))

        detailsView.buttonView.updateView(with: ButtonViewConfiguration(title: "COMPRAR", action: { [weak self] in
            self?.viewModel.purchaseRandomProduct()
        }))

        Service.setImageViewByUrl(detailsView.image, String.getThumbnailFullPath(comic.thumbnail.path, comic.thumbnail.extension))
        detailsView.updateView(with: DetailsViewConfiguration(title: comic.title, issue: comic.issueNumber, prices: String.getPrices(comic.prices)))
    }

    func favoriteFeedback(is favorite: Bool) {
        if favorite {
            detailsView.favoriteImage.image = UIImage(systemName: "heart.fill")
            detailsView.favoriteImage.tintColor = .systemRed
        } else {
            detailsView.favoriteImage.image = UIImage(systemName: "heart")
            detailsView.favoriteImage.tintColor = .systemGray
        }
    }
}
