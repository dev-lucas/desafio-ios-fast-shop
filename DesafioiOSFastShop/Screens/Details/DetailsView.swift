//
//  DetailsView.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 26/05/23.
//

import UIKit

protocol DetailsViewDelegate: AnyObject {
    func favoriteComic()
}

struct DetailsViewConfiguration {
    let title: String
    let issue: Double
    let prices: String
}

class DetailsView: UIView {

    weak var delegate: DetailsViewDelegate?

    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 15
        stackView.backgroundColor = .systemBackground
        return stackView
    }()

    private(set) lazy var image: UIImageView = {
        let image = UIImageView(image: UIImage())
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.backgroundColor = .systemBackground
        return image
    }()

    private(set) lazy var favoriteImage: UIImageView = {
        let image = UIImageView(image: UIImage(systemName: "heart"))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(favoriteImageTapped))
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.backgroundColor = .systemBackground
        image.tintColor = .systemGray
        image.isUserInteractionEnabled = true
        image.addGestureRecognizer(tapGesture)
        return image
    }()

    private lazy var auxiliaryStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 20
        stackView.distribution = .fill
        stackView.alignment = .leading
        stackView.backgroundColor = .systemBackground
        return stackView
    }()

    private lazy var auxiliaryButtonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 10
        stackView.distribution = .fill
        stackView.backgroundColor = .systemBackground
        return stackView
    }()

    private lazy var mainInfoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.backgroundColor = .systemBackground
        return stackView
    }()

    private lazy var priceInfoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.backgroundColor = .systemBackground
        return stackView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: UIFont.preferredFont(forTextStyle: .body).pointSize, weight: .semibold)
        label.textAlignment = .left
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()

    private lazy var issueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: UIFont.preferredFont(forTextStyle: .callout).pointSize, weight: .semibold)
        label.textAlignment = .left
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()

    private lazy var pricesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: UIFont.preferredFont(forTextStyle: .caption2).pointSize, weight: .regular)
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()

    lazy var buttonView: ButtonView = {
        let button = ButtonView()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.contentMode = .scaleAspectFill
        return button
    }()

    lazy var cartButtonView: ButtonView = {
        let button = ButtonView()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.contentMode = .scaleAspectFill
        button.button.setTitle("", for: .normal)
        button.button.setImage(UIImage(systemName: "cart.fill"), for: .normal)
        return button
    }()

    init() {
        super.init(frame: .zero)
        self.setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension DetailsView {

    func setupViews() {
        self.backgroundColor = .systemBackground
        self.configureSubviews()
        self.configureSubviewsConstraints()
    }

    func configureSubviews() {
        addSubview(mainStackView)
        mainStackView.addArrangedSubview(image)
        mainStackView.addArrangedSubview(auxiliaryStackView)
        auxiliaryStackView.addArrangedSubview(mainInfoStackView)
        mainInfoStackView.addArrangedSubview(titleLabel)
        mainInfoStackView.addArrangedSubview(issueLabel)
        auxiliaryStackView.addArrangedSubview(priceInfoStackView)
        priceInfoStackView.addArrangedSubview(pricesLabel)
        auxiliaryStackView.addArrangedSubview(favoriteImage)
        auxiliaryStackView.addArrangedSubview(auxiliaryButtonStackView)
        auxiliaryButtonStackView.addArrangedSubview(buttonView)
        auxiliaryButtonStackView.addArrangedSubview(cartButtonView)
    }

    func configureSubviewsConstraints() {
        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 15),
            mainStackView.heightAnchor.constraint(equalToConstant: 250),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            image.heightAnchor.constraint(equalToConstant: 250),
            image.widthAnchor.constraint(equalToConstant: 150),
            favoriteImage.widthAnchor.constraint(equalToConstant: 25),
            favoriteImage.heightAnchor.constraint(equalToConstant: 25),
            pricesLabel.widthAnchor.constraint(equalTo: auxiliaryStackView.widthAnchor),
            buttonView.heightAnchor.constraint(equalToConstant: 40),
            cartButtonView.heightAnchor.constraint(equalToConstant: 40),
            cartButtonView.widthAnchor.constraint(equalToConstant: 40),
            buttonView.widthAnchor.constraint(equalTo: auxiliaryStackView.widthAnchor, constant: -50)
        ])
    }

    @objc private func favoriteImageTapped() {
        delegate?.favoriteComic()

        UIView.animate(withDuration: 0.2, animations: {
            self.favoriteImage.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }, completion: { _ in
            UIView.animate(withDuration: 0.2, animations: {
                self.favoriteImage.transform = .identity
            })
        })
    }
}

extension DetailsView {
    func updateView(with configuration: DetailsViewConfiguration) {
        titleLabel.text = configuration.title
        issueLabel.text = String.getIssue(configuration.issue)
        pricesLabel.text = configuration.prices
    }
}
