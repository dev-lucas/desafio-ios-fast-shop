//
//  DetailsViewModel.swift
//  DesafioiOSFastShop
//
//  Created by Lucas Gomes on 28/05/23.
//

import UIKit

protocol DetailsViewModelDelegate: AnyObject {
    func didUpdateFavoriteStatus(isFavorite: Bool)
}

class DetailsViewModel {
    var comic: MarvelComicsDataResultsModel?
    private var isFavorite = false

    weak var delegate: DetailsViewModelDelegate?

    init(comic: MarvelComicsDataResultsModel?) {
        self.comic = comic
    }

    var isComicFavorite: Bool {
        return isFavorite
    }

    func setupDetails(completion: (Bool) -> Void) {
        guard let comic = comic else { return }

        if MarvelComicsDataManager.shared.getFavorites(MarvelComicsDataManagerModel(title: comic.title, issueNumber: comic.issueNumber, price: nil)) != nil {
            isFavorite = true
        } else {
            isFavorite = false
        }

        completion(isFavorite)
    }

    func toggleFavoriteComic() {
        guard let comic = comic else { return }

        if isFavorite {
            MarvelComicsDataManager.shared.deleteFavorites(MarvelComicsDataManagerModel(title: comic.title, issueNumber: comic.issueNumber, price: nil))
            isFavorite = false
        } else {
            MarvelComicsDataManager.shared.saveFavorites(MarvelComicsDataManagerModel(title: comic.title, issueNumber: comic.issueNumber, price: nil))
            isFavorite = true
        }

        delegate?.didUpdateFavoriteStatus(isFavorite: isFavorite)
    }

    func purchaseRandomProduct() {
        if let product = StoreManager.shared.products.randomElement() {
            StoreManager.shared.purchaseProduct(product)
        }
    }
}
