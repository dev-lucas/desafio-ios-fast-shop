# Desafio iOS Fast Shop

O Desafio iOS Fast Shop é um aplicativo desenvolvido em Swift utilizando UIKit. Foi criado como parte de um desafio de desenvolvimento e tem como objetivo listar HQ's da Marvel. O aplicativo foi desenvolvido seguindo a arquitetura MVVM e utilizando layout em View Code. Além disso, o projeto contém testes unitários XCTests para garantir a qualidade e o bom funcionamento do código.

## Sumário

- [Requisitos de Sistema](#requisitos-de-sistema)
- [Instalação](#instalação)
- [Executando o Aplicativo](#executando-o-aplicativo)
- [Funcionalidades](#funcionalidades)
- [Informações de Login](#informações-de-login)

## Requisitos de Sistema

- O aplicativo requer iOS 13 ou versão posterior para ser executado.
- O dispositivo de destino deve estar executando uma versão compatível do iOS.

## Instalação

Para instalar o Desafio iOS Fast Shop em seu ambiente local, siga as etapas abaixo:

1. Clone o repositório usando o seguinte comando:

```bash
git clone https://gitlab.com/dev-lucas/desafio-ios-fast-shop.git
```

2. Navegue até o diretório do projeto:

```bash
cd desafio-ios-fast-shop
```

3. Instale as dependências usando o Cocoapods. Certifique-se de ter o Cocoapods instalado em seu sistema. Se você não tiver, consulte [https://cocoapods.org](https://cocoapods.org) para obter instruções de instalação.

```bash
pod install
```

Após a conclusão dessas etapas, você terá o Desafio iOS Fast Shop instalado localmente em seu ambiente.

## Executando o Aplicativo

Antes de executar o aplicativo, verifique se seu dispositivo de destino está conectado ou se o simulador está em execução.

1. Abra o arquivo `DesafioFastShop.xcworkspace` no Xcode.

2. Selecione o dispositivo de destino ou o simulador de sua escolha.

3. Pressione o botão de "Executar" (ícone de reprodução) ou use o atalho de teclado `Cmd + R` para iniciar a compilação e execução do aplicativo.

4. O aplicativo será compilado e iniciado no dispositivo de destino selecionado ou no simulador.

Agora você pode explorar e testar o Desafio iOS Fast Shop em seu ambiente local.

## Funcionalidades

O Desafio iOS Fast Shop oferece as seguintes funcionalidades:

- Listagem de HQ's da Marvel
- Favoritar e desfavoritar HQ's
- Filtro de ano para a listagem de HQ's
- Busca pelo título da comic
- Scroll infinito na tela principal
- Pull-to-refresh na tela principal
- Carrinho para simular compras
- Simulação de compra (In-App Purchase)

## Dependências

O Desafio iOS Fast Shop utiliza as seguintes dependências:

- SwiftLint: Ferramenta de linting para manter um estilo de código consistente em Swift.
- Alamofire: Biblioteca para simplificar requisições de rede HTTP.
- AlamofireImage: Biblioteca que fornece suporte para carregar e exibir imagens usando Alamofire.

Certifique-se de que todas as dependências estejam corretamente instaladas antes de executar o aplicativo.

## Informações de Login

Para acessar o aplicativo, utilize as seguintes informações de login:

- Email: teste@teste
- Senha: 123

**Observação:** Essas informações de login são mockadas e fornecidas apenas para fins de teste.
