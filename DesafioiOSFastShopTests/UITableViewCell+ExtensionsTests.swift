//
//  UITableViewCell+ExtensionsTests.swift
//  DesafioiOSFastShopTests
//
//  Created by Lucas Gomes on 27/05/23.
//

import XCTest
@testable import DesafioiOSFastShop

final class UITableViewCell_ExtensionsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testClassIdentifierExtension() throws {
        XCTAssertEqual("CellView", CellView.classIdentifier())
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
