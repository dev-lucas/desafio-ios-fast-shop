//
//  String+ExtensionsTests.swift
//  DesafioiOSFastShopTests
//
//  Created by Lucas Gomes on 27/05/23.
//

import XCTest
@testable import DesafioiOSFastShop

final class String_ExtensionsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetPricesExtension() throws {
        let prices = [
            MarvelComicsDataResultsPriesModel(type: "printPrice", price: 10),
            MarvelComicsDataResultsPriesModel(type: "digitalPurchasePrice", price: 20)
        ]

        let result = String.getPrices(prices)
        XCTAssertEqual("Price: $10.0\nDigital Price: $20.0\n", result)
    }
    
    
    func testGetThumbnailFullPathExtension() throws {
        let path = "http://teste123"
        let pathExtension = "jpg"
        
        let result = String.getThumbnailFullPath(path, pathExtension)
        XCTAssertEqual("https://teste123.jpg", result)
    }
    
    func testGetIssueExtension() throws {
        let issue = 12.0
        
        let result = String.getIssue(issue)
        XCTAssertEqual("Issue #12", result)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
